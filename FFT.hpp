
/*
	This FFT has been proposed by Paul Bourke 
	http://paulbourke.net/miscellaneous/dft/
	This computes an in-place complex-to-complex FFT 
	x and y are the real and imaginary arrays of 2^m points.
	dir =  1 gives forward transform
	dir = -1 gives reverse transform 
	You MUST compute first the value m such that
	2^(m-1) < n (size of your signal) <= 2^m
	allocate a new signal of nm=2^m values
	then fill the n first values of this new signal 
 with your signal and fill the rest with 0
	WARNING : you must pass m, not nm !!!
	*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <pthread.h>

#include "Wave.h"

struct FourrierData
{
	double* real_part;
	double* img_part;
	int size;

	FourrierData(){}

	FourrierData(int s,double* r,double* i)
	{
		size = s;
		real_part = r;
		img_part = i;
	}

	FourrierData(int s)
	{
		size = s;
		real_part = new double[s];
		img_part = new double[s];
	}
};

void FourrierToData(FourrierData fourrier,double* data)
{
	double maxVal = 0;

	for(int k = 0; k < fourrier.size; k++)
	{
		data[k] = sqrt( (fourrier.real_part[k]*fourrier.real_part[k]) + (fourrier.img_part[k] * fourrier.img_part[k]));

		maxVal = std::max(maxVal, abs(data[k]) );
	}
	for(int k = 0; k < fourrier.size; k++)
	{
		data[k] /= maxVal;
	}
}

struct FTdata
{
	double* signal;
	int count;
	FourrierData fourrier;
	int k;
};

void * DFTthread( void* arg)
{	
	struct FTdata * data = (struct FTdata*)arg;

	double* signal = data->signal;
	int k = data->k;
	FourrierData &fourrier = data->fourrier;
	int count = data->count;

	double kdpi = ((2 * M_PI) / count) * k;

	fourrier.real_part[k] = 0;
	fourrier.img_part[k] = 0;

	for(int n = 0; n < count; n++)
	{
		fourrier.real_part[k] += signal[n] * cos(kdpi * n);
		fourrier.img_part[k] += signal[n] * sin(kdpi * n);
	}
		
	fourrier.img_part[k] *= -1;
}

void DFT(double* signal,int count,FourrierData &fourrier,int threadCount)
{
	pthread_t threads[threadCount];
	struct FTdata * datas = (struct FTdata *)malloc(sizeof(struct FTdata) * threadCount);

	for(int k = 0; k < count; k++)
	{
		if(k < threadCount)
		{
			datas[k%threadCount].signal = signal;
			datas[k%threadCount].count = count;
			datas[k%threadCount].fourrier = fourrier;
			datas[k%threadCount].k = k;			

			pthread_create(&threads[k%threadCount],NULL,DFTthread,&datas[k%threadCount]);
		}
		else
		{
			pthread_join(threads[k%threadCount],NULL);

			datas[k%threadCount].k = k;

			pthread_create(&threads[k%threadCount],NULL,DFTthread,&datas[k%threadCount]);
		}
	}

	for(int i = 0 ; i < threadCount;i++)
	{
		pthread_join(threads[i],NULL);
	}
}


void DFT(double* signal,int count,FourrierData & fourrier)
{
	double dpi = (2 * M_PI) / count;

	for(int k = 0; k < count; k++)
	{
		double kdpi = dpi * k;

		fourrier.real_part[k] = 0;
		fourrier.img_part[k] = 0;

		for(int n = 0; n < count; n++)
		{
			fourrier.real_part[k] += signal[n] * cos(kdpi * n);
			fourrier.img_part[k] += signal[n] * sin(kdpi * n);
		}
		fourrier.img_part[k] *= -1;
	}
}

void IDFT(FourrierData & signal,int count,FourrierData & fourrier)
{
	double dpi = (2 * M_PI) / count;
	
	for(int n = 0; n < count; n++)
	{			
		double kdpi = dpi * n;

		signal.real_part[n] = 0;
		signal.img_part[n] = 0;

		for(int k = 0; k < count; k++)
		{
			signal.real_part[n] += (fourrier.real_part[k] * cos(kdpi * k)) - (fourrier.img_part[k] * sin(kdpi * k));
			//signal.img_part[n] += (fourrier.real_part[k] * sin(kdpi * k)) + (fourrier.img_part[k] * cos(kdpi * k));
		}
		signal.real_part[n] /= count;
		//signal.img_part[n] /= count;
		signal.img_part[n] = 0;
	}
}

//==================================================================================================================
//==================================================================================================================

int FFTmethod(int dir,int m,double *x,double *y)
{
	int n,i,i1,j,k,i2,l,l1,l2;
	double c1,c2,tx,ty,t1,t2,u1,u2,z;
	
	/* Calculate the number of points */
	n = 1;
	for (i=0;i<m;i++) 
		n *= 2;
	
	/* Do the bit reversal */
	i2 = n >> 1;
	j = 0;
	for (i=0;i<n-1;i++) {
		if (i < j) {
			tx = x[i];
			ty = y[i];
			x[i] = x[j];
			y[i] = y[j];
			x[j] = tx;
			y[j] = ty;
		}
		k = i2;
		while (k <= j) {
			j -= k;
			k >>= 1;
		}
		j += k;
	}
	
	/* Compute the FFT */
	c1 = -1.0; 
	c2 = 0.0;
	l2 = 1;
	for (l=0;l<m;l++) {
		l1 = l2;
		l2 <<= 1;
		u1 = 1.0; 
		u2 = 0.0;
		for (j=0;j<l1;j++) {
			for (i=j;i<n;i+=l2) {
				i1 = i + l1;
				t1 = u1 * x[i1] - u2 * y[i1];
				t2 = u1 * y[i1] + u2 * x[i1];
				x[i1] = x[i] - t1; 
				y[i1] = y[i] - t2;
				x[i] += t1;
				y[i] += t2;
			}
			z =  u1 * c1 - u2 * c2;
			u2 = u1 * c2 + u2 * c1;
			u1 = z;
		}
		c2 = sqrt((1.0 - c1) / 2.0);
		if (dir == 1) 
			c2 = -c2;
		c1 = sqrt((1.0 + c1) / 2.0);
	}
	
	/* Scaling for forward transform */
	if (dir == 1) {
		for (i=0;i<n;i++) {
			x[i] /= n;
			y[i] /= n;
		}
	}
	
	return(1);
}

FourrierData DFFT(double* signal,int size)
{
	int m = ceil(log2(size)); 

	int s = pow(2,m);

	FourrierData fourrier = FourrierData(s);
	for(int i = 0; i < s;i++)
	{
		fourrier.real_part[i] = (i < size) ? signal[i] : 0;
		fourrier.img_part[i] = 0;
	}

	FFTmethod(1,m,fourrier.real_part,fourrier.img_part);

	return fourrier;
}

void IFFT(FourrierData& fourrier,double* data,int dataCount)
{
	int m = log2(fourrier.size);

	double* x = new double[fourrier.size];
	double* y = new double[fourrier.size];
	for (int i = 0; i < fourrier.size; i++)
	{
		x[i] = fourrier.real_part[i];
		y[i] = fourrier.img_part[i];
	}

	FFTmethod(-1,m,x,y);

	for(int i = 0; i < dataCount;i++)
	{
		data[i] = x[i];
	}
}

double getStep(FourrierData fourrier,double frequency, double sampling_freq)
{
	int halfsize = (fourrier.size*0.5);
	double step = sampling_freq / halfsize;

	int target = 0; double dist = 1000;
	for(int i = 0; i < halfsize;i++)
	{
		double d = abs((i*step)-frequency);
		if(d < dist)
		{
			target = i; dist = d;
		}
	}

	return target;
}

/*
double* FilterButterworth(double* signal,int size, double fc,double sampling_freq)
{
	double* output = new double[size];

	double wc = 2 * M_PI * fc;
	double T = 1.0 / sampling_freq;
	double a = M_PI * (fc/sampling_freq);
	double a2 = a*a; double a3 = a2*a;

	double A = 1 + 2*a + 2*a*a + 3*a*a*a;
	double B = -3 - 2*a + 2*a*a + 3*a*a*a;
	double C = 3 - 2*a - 2*a*a + 3*a*a*a;
	double D = -1 + 2*a - 2*a*a + a*a*a;


	for(int i = 0; i < size;i++)
	{
		double by = i > 0 ? (B*output[i-1]) : 0;
		double cy = i > 1 ? (C*output[i-2]) : 0;
		double dy = i > 2 ? (D*output[i-3]) : 0;
		double x3 = i > 2 ? (a3 * signal[i-3]) : 0;
		double x2 = i > 1 ? (3 * a3 * signal[i-2]) : 0;
		double x1 = i > 0 ? (3 * a3 * signal[i-1]) : 0;
		double x = signal[i];

		output[i] = -by - cy - dy + x3 + x2 + x1 + x ;
		output[i] /= A;
	}

	return output;
}
*/

double* FiltreButterworth(double* signal, int dataCount, double fe, double fc)
{
	double* _filtred = new double[dataCount];

    double alpha = M_PI*fc/fe;
    double a[4], b[4];
    b[0] = alpha*alpha*alpha;
    b[3] = b[0];
    b[1] = 3.0* b[0];
    b[2] = b[1];

	double A = 1 + 2*alpha + 2*alpha*alpha + b[0];
    double B = -3 - 2*alpha + 2*alpha*alpha + b[1];
    double C = 3 - 2*alpha - 2*alpha*alpha + b[1];
    double D = -1 + 2*alpha - 2*alpha*alpha + b[0];

    a[0] = 0.0;
    a[1] = -B/A;
    a[2] = -C/A;
    a[3] = -D/A;

    b[0] /= A;
    b[1] /= A;
    b[2] /= A;
    b[3] /= A;

    for(int n = 0; n<dataCount; n++)
    {
		double _yn = 0;
		for(int k = 0; k<4; k++)
		{
			int m = n-k;
			if(m>=0) _yn += b[k]*signal[m] + a[k]*_filtred[m];
		}
        _filtred[n] = _yn;
    }

    return _filtred;
}


void FilterPasseBasHaut(FourrierData& fourrier,double frequency,double sampling_freq,bool haut)
{
	int hs = fourrier.size * 0.5;
	int target = getStep(fourrier,frequency,sampling_freq);

	for(int i = 0; i < hs;i++)
	{
		if(haut)
		{
			double v = (i < target) ? 0 : 1;
			fourrier.real_part[i] *= v;
			fourrier.img_part[i] *= v;
			fourrier.real_part[fourrier.size-i] *= v;
			fourrier.img_part[fourrier.size-i] *= v;
		}
		else{
			double v = (i > target) ? 0 : 1;
			fourrier.real_part[i] *= v;
			fourrier.img_part[i] *= v;
			fourrier.real_part[fourrier.size-i] *= v;
			fourrier.img_part[fourrier.size-i] *= v;
		}
	}
}

FourrierData Transpose(FourrierData fourrier,double transposeFrequency,double sampling_freq)
{
	FourrierData outFourrier = FourrierData(fourrier.size);

	int hs = fourrier.size * 0.5;
	int delta = getStep(fourrier,abs(transposeFrequency),sampling_freq);

	int from = transposeFrequency > 0 ? delta : 0;
	int to = transposeFrequency < 0 ? hs-delta : hs;

	for(int i = 0; i < hs;i++)
	{
		if(i < from || i >= to){
			outFourrier.real_part[i] = 0;
			outFourrier.img_part[i] = 0;
			outFourrier.real_part[fourrier.size-i] = 0;
			outFourrier.img_part[fourrier.size-i] = 0;
		}
		else if(transposeFrequency < 0){
			outFourrier.real_part[i] = fourrier.real_part[i+delta];
			outFourrier.img_part[i] = fourrier.img_part[i+delta];
			outFourrier.real_part[fourrier.size-i] = fourrier.real_part[fourrier.size-i-delta];
			outFourrier.img_part[fourrier.size-i] = fourrier.img_part[fourrier.size-i-delta];
		}
		else{
			outFourrier.real_part[i] = fourrier.real_part[i-delta];
			outFourrier.img_part[i] = fourrier.img_part[i-delta];
			outFourrier.real_part[fourrier.size-i] = fourrier.real_part[fourrier.size-i+delta];
			outFourrier.img_part[fourrier.size-i] = fourrier.img_part[fourrier.size-i+delta];
		}
	}

	return outFourrier;
}

//==================================================================================================================
//==================================================================================================================

double toDouble(unsigned char value)
{
	return (double)((value-127.5) / 127.5);
}

unsigned char toChar(double value)
{
	if(value < -1.0){return (unsigned char)0;}
	if(value > 1.0) {return (unsigned char)255;}
	return (unsigned char)floor((value+1.0)*127.5);
}

void toDoubleArray(unsigned char * data,int size,double** arr)
{
	for(int i = 0; i < size;i++)
	{
		(*arr)[i] = toDouble(data[i]);
	}
}

void toCharArray(double* data,int size, unsigned char** arr)
{
	for(int i = 0; i < size;i++)
	{
		(*arr)[i] = toChar(data[i]);
	}
}
