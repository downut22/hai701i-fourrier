
/*
	This FFT has been proposed by Paul Bourke 
	http://paulbourke.net/miscellaneous/dft/
	This computes an in-place complex-to-complex FFT 
	x and y are the real and imaginary arrays of 2^m points.
	dir =  1 gives forward transform
	dir = -1 gives reverse transform 
	You MUST compute first the value m such that
	2^(m-1) < n (size of your signal) <= 2^m
	allocate a new signal of nm=2^m values
	then fill the n first values of this new signal 
 with your signal and fill the rest with 0
	WARNING : you must pass m, not nm !!!
	*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <chrono>

using namespace std::chrono;

#define FREQ 44100.0

#include "FFT.hpp"

void synthetiseNote(FourrierData& fourrier,double frequency,double sampling_freq,double value)
{
	int i = getStep(fourrier,frequency,sampling_freq);

	fourrier.real_part[i] = fourrier.img_part[i] = value;
	fourrier.real_part[fourrier.size-i] = fourrier.img_part[fourrier.size-i] = value;
}

void generateNote(double* data,int dataCount,double waveLenght,double note,double amplitude)
{
	double val = waveLenght * note * M_PI;

	for(int i = 0; i < dataCount;i++)
	{
		data[i] += sin(i*val) * amplitude;
	}
}

void process(double** data,double duration,double sampling_freq,int * dataCount)
{	
	int count = (int)(duration * sampling_freq);
	duration = count / sampling_freq;

	(*data) = new double[count]; for(int i = 0 ; i < count;i++){(*data)[i] = 0;}

	char cinn[20];
	std::cout << "Enter numbre of notes " <<std::endl;
	std::cin >> cinn;int noteCount = atoi(cinn);
	double * notes = new double[noteCount];

	for(int i = 0; i < noteCount;i++)
	{
		std::cout << " Note n° " << i << " frequency : ";
		std::cin >> cinn;notes[i] = atof(cinn);
	}

	for(int i = 0; i < noteCount;i++)
	{
		generateNote((*data),count,1.0/sampling_freq,notes[i],0.5);
	}

	(*dataCount) = count;
}

int main(int argc, char** argv)
{
	char* input; char* output; char* fourrierOutput;
	double * data; unsigned char* dataRaw; int dataCount; 
	int sampling_freq; bool useFFT;

	if(argc < 4)
	{ 
		std::cout << "USAGE : " << std::endl << 
		"  >>  ./a.out [1 for FFT] [inputFile] [outputFile] [fourierFile] " << std::endl << 
		"or" << std::endl <<
		"  >>  ./a.out [1 for FFT] [outputFile] [fourierFile] ";
		return 0;
	}
	else if(argc == 4)
	{
		useFFT = atoi(argv[1]) == 1;
		output = argv[2];
		fourrierOutput = argv[3];

		char cinn[20];
		std::cout << "Enter sampling frequency (usually between 10000 and 50000, standard 441000) : "<<std::endl;
		std::cin >> cinn; sampling_freq = atoi(cinn);

		std::cout << "Enter duration : "<<std::endl;
		std::cin >> cinn;double duration = atof(cinn);

		std::cout << "Processing data with " << sampling_freq << " Hz , duration = " << duration << " ..." << std::endl << std::endl;
		
		process(&data,duration,sampling_freq,&dataCount);
	}
	else //argc == 5
	{
		useFFT = atoi(argv[1]) == 1;
		input = argv[2];
		output = argv[3];
		fourrierOutput = argv[4];

		std::cout << "Opening file " << input << std::endl << std::endl;

		Wave inputFile = Wave();
		inputFile.read(input);

		std::cout << std::endl << "File loaded successfully"<< std::endl;

		inputFile.getData8(&dataRaw,&dataCount);

		sampling_freq = inputFile.samplingFreq(); 
		double sampling_lenght = 1.0 / (double)(sampling_freq);
		double duration = sampling_lenght * dataCount;

		data = new double[dataCount]; 
		toDoubleArray(dataRaw,dataCount,&data);

		std::cout<< "Data size : " << dataCount << 
		" | Wavelenght ; Frequency ; Duration : " << sampling_lenght << " ; " << sampling_freq << " ; " << duration <<
		std::endl << std::endl;
	}
	
	//==================================================================================================================
	//==================================================================================================================

	std::cout << "Processing Fourrier Tranform " << (useFFT ? "using FFT" : "") << "..." << std::endl << std::endl;

	auto start = high_resolution_clock::now();

	FourrierData fourrier = FourrierData();
	if(useFFT)
	{
		fourrier = DFFT(data,dataCount);
	}
	else
	{	
		fourrier = FourrierData(dataCount);
		DFT(data,dataCount,fourrier,100);
	}

	auto stop = high_resolution_clock::now();

	auto duration = duration_cast<milliseconds>(stop-start);

	std::cout << "	>> Took : " << duration.count() << std::endl;

	//==================================================================================================================
	//==================================================================================================================
	
	std::cout <<"Filtering fourrier " << std::endl;

	char cinn[20];double filv;

	std::cout << "Enter passe-haut frequency (0 to ignore) : ";
	std::cin>>cinn; filv = atof(cinn);
	if(filv > 0) FilterPasseBasHaut(fourrier,filv,sampling_freq,true);

	std::cout << "Enter passe-bas frequency (0 to ignore) : ";
	std::cin>>cinn; filv = atof(cinn);
	if(filv > 0) FilterPasseBasHaut(fourrier,filv,sampling_freq,false);

	std::cout << "Enter transposition frequency (0 to ignore) : ";
	std::cin>>cinn; filv = atof(cinn);
	if(filv > 0) fourrier = Transpose(fourrier,filv,sampling_freq);

	//synthetiseNote(fourrier,880,sampling_freq,0.8);

	//==================================================================================================================
	//==================================================================================================================

	std::cout <<"Writing fourrier data in file " << fourrierOutput << std::endl;

	double* fourrierData = new double[fourrier.size];
	FourrierToData(fourrier,fourrierData);

	dataRaw = new unsigned char[fourrier.size];  
	toCharArray(fourrierData,fourrier.size,&dataRaw);

	Wave outputFile = Wave(dataRaw,fourrier.size,1,sampling_freq);
	outputFile.write(fourrierOutput);

	//==================================================================================================================
	//==================================================================================================================

	std::cout << "Processing Inverse Fourrier Tranform " << (useFFT ? "using FFT" : "") << "..." << std::endl << std::endl;

	FourrierData fourrierSignal = FourrierData(dataCount);
	if(useFFT)
	{
		IFFT(fourrier,data,dataCount);
	}
	else
	{
		IDFT(fourrierSignal,dataCount,fourrier);
		data = fourrierSignal.real_part;
	}

	//==================================================================================================================
	//==================================================================================================================

	std::cout <<"Filtering signal" << std::endl;

	std::cout << "Enter butterworth frequency (0 to ignore) : ";
	std::cin>>cinn; filv = atof(cinn);
	if(filv > 0) data = FiltreButterworth(data,dataCount,sampling_freq,filv);//FilterButterworth(data,dataCount,filv,sampling_freq);

	//==================================================================================================================
	//==================================================================================================================

	std::cout <<"Writing back data in file " << output << std::endl;

	dataRaw = new unsigned char[dataCount];  
	toCharArray(data,dataCount,&dataRaw);

	outputFile = Wave(dataRaw,dataCount,1,sampling_freq);
	outputFile.write(output);

	delete[] data,dataRaw;

	return 0;
}
